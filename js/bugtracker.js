jQuery(function(){

  (function($){
    var Tracker

    Tracker = function (){
      var el = $("<div id='bugtracker'>").appendTo('body');
      var btn = $("<button>").html("Show List").appendTo(el);

      $(btn).bind( "click", function() {
        el.toggleClass("opened");
        btn.toggleClass("clicked")
      });

      var list = $("<ul class='buglist'>").appendTo(el).hide();
      listView = new TrackerList(list);
    }

    
    TrackerList = function(el){

      $.ajax({
        url: "https://still-ridge-1849.herokuapp.com/bugs.json?api_key=6a4f2c952b3ccbc8924b89b7f5c3d0dc",
        jsonp: "callback",
        dataType: "jsonp"
      });

      // ToDo rename 'callback for smth strict'
      callback = function(data) {
        var text = '';
        var len = data.length;
        for(var i=0;i<len;i++){
            bug = data[i];
            console.log(bug);
            text += "<li><strong>" + bug['name'] + "</strong>" + bug['description'] + "</li>";
        };

        $(el).html(text);
        $(el).show();
      };
    };


    t = new Tracker();

  })(jQuery)

});
